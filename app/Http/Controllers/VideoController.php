<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Video;
use App\Repositories\User;
use App\Http\Requests\VideoUpdateRequest;

class VideoController extends Controller
{
    public function totalSize($user){
        return ['total' => app()->make(User::class)->getVideosTotalSize($user->id)];
    }

    public function show(Request $request, $video){
        return app()->make(Video::class)->find($video->id, $request->with ? : []);
    }

    public function update(VideoUpdateRequest $request, $video){
        return [
            'success' => app()->make(Video::class)->put($video->id, $request->toArray())
        ];
    }
}
