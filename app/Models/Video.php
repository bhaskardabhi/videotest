<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = [
        'size', 'viewer_count'
    ];

    protected $casts = [
        'size' => 'float',
    ];

    public function createdBy(){
        return $this->belongsTo(User::class,'user_id');
    }
}
