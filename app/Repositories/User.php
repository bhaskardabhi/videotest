<?php

namespace App\Repositories;

use App\Models\User as UserModel;

class User {

    public function model()
    {
        return new UserModel;
    }

    public function getVideosTotalSize($id){
        return app()->make(Video::class)->getVideosTotalSizeByUser($id);
    }
}