<?php

namespace App\Repositories;

use App\Models\Video as VideoModel;

class Video {

    public function model()
    {
        return new VideoModel;
    }

    public function getVideosTotalSizeByUser(int $userId)
    {
        return (float) $this->model()->whereUserId($userId)->sum('size');
    }

    public function find(int $id, array $with = []){
        return $this->model()->with($with)->find($id);
    }

    public function put($id, array $data){
        $video = $this->find($id);
        $video->fill($data);
        return $video->save();
    }
}
