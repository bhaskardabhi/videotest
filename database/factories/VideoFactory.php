<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Video;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Video::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'slug' => $faker->slug,
        'size' => rand(10,1000),
        'viewer_count' => rand(10,1000),
        'user_id' => $faker->randomElement(User::all()),
    ];
});
