<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        return DB::transaction(function (){
            $slugs = ['User1','User2','User3'];
    
            foreach($slugs as $slug){
                $name = Str::random(10);

                $user = new User;
                $user->slug = $slug;
                $user->name = $name;
                $user->save();
            }

            return true;
        });
    }
}
