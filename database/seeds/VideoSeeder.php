<?php

use Illuminate\Database\Seeder;
use App\Models\Video;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class VideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        return DB::transaction(function (){
            $videos = [
                ['index' => 1,'user' => 'User1','size' => 120,'viewer_count' => 1100],
                ['index' => 2,'user' => 'User1','size' => 80,'viewer_count' => 2000],
                ['index' => 3,'user' => 'User1','size' => 250,'viewer_count' => 900],
                ['index' => 4,'user' => 'User2','size' => 90,'viewer_count' => 600],
                ['index' => 5,'user' => 'User2','size' => 75,'viewer_count' => 700],
                ['index' => 6,'user' => 'User2','size' => 300,'viewer_count' => 3000],
                ['index' => 7,'user' => 'User3','size' => 200,'viewer_count' => 2200],
            ];

            foreach($videos as $video){
                $model = new Video;
                $model->slug = 'Video'.$video['index'];
                $model->name = Str::random(10);
                $model->size = $video['size'];
                $model->viewer_count = $video['viewer_count'];
                $model->user_id = User::whereSlug($video['user'])->first()->id;
                $model->save();
            }

            return true;
        });
    }
}
