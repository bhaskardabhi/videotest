# Setup

Clone Repo
Run Migration using `php artisan migrate`
Run DB Seeder if in Local environment using `php artisan db:seed`
Run Tests using `phpunit`

# APIs

APIs are as below. Also Note, `{user}` or `{video}` parameter can be used for passing `id` or `slug` (Both will work). I have added Swagger file in project.

## A GET endpoint that returns total Videos Size created by a user

Method: GET
URL: api/v1/users/{user}/videos/total-size

## A GET endpoint for Video metadata

Method: GET
URL: api/v1/videos/{video}

### Get with creator of the video

Method: GET
URL: api/v1/videos/{video}?with[]=createdBy

## A PATCH endpoint that updates Video size and Viewers Count

Method: GET
URL: api/v1/videos/{video}