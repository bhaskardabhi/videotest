<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::get('users/{user}/videos/total-size', 'VideoController@totalSize');
    Route::get('videos/{video}', 'VideoController@show');
    Route::put('videos/{video}', 'VideoController@update');
});
