<?php

namespace Tests\Http;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;
use App\Models\Video;
use App\Repositories\Video as VideoRepo;

class VideoApiTest extends TestCase
{
    public function test_that_correct_total_video_size_returned_for_user()
    {
        $user = User::has('videos')->inRandomOrder()->first();

        if(!$user){
            $user = User::inRandomOrder()->first();
        }

        $response = $this->get('/api/v1/users/'.$user->slug."/videos/total-size");
        $response->assertStatus(200)->assertJson(['total' => $user->videos()->sum('size')]);

        $response = $this->get('/api/v1/users/'.$user->id."/videos/total-size");
        $response->assertStatus(200)->assertJson(['total' => $user->videos()->sum('size')]);
    }

    public function test_that_video_information_is_returned()
    {
        $video = Video::inRandomOrder()->first();

        $response = $this->get('/api/v1/videos/'.$video->slug);
        $response->assertStatus(200)->assertJson($video->toArray());
        $response = $this->get('/api/v1/videos/'.$video->id);
        $response->assertStatus(200)->assertJson($video->toArray());

        $response = $this->get('/api/v1/videos/'.$video->slug."?with[]=createdBy");
        $response->assertStatus(200)->assertJson($video->load(['createdBy'])->toArray());
        $response = $this->get('/api/v1/videos/'.$video->id."?with[]=createdBy");
        $response->assertStatus(200)->assertJson($video->load(['createdBy'])->toArray());
    }

    public function test_that_video_is_updated_on_patch_request()
    {
        $oldModel = factory(Video::class)->create();
        
        //// Checking with slug
        $request = ['size' => rand(10,100),'viewer_count' =>  rand(10,1000)];

        // Checking if Backend says that model is updated successfully
        $response = $this->json('PUT','/api/v1/videos/'.$oldModel->slug,$request);
        $response->assertStatus(200)->assertJson(['success' => true]);
        // Checking if really model is updated
        $newModel = app()->make(VideoRepo::class)->find($oldModel->id);

        $this->assertEquals($newModel->size, $request['size']);
        $this->assertEquals($newModel->viewer_count, $request['viewer_count']);

        //// Checking with id
        $oldModel = $newModel;
        $request = ['size' => rand(10,100),'viewer_count' =>  rand(10,1000)];

        // Checking if Backend says that model is updated successfully
        $response = $this->json('PUT','/api/v1/videos/'.$oldModel->id,$request);
        $response->assertStatus(200)->assertJson(['success' => true]);

        // Checking if really model is updated
        $newModel = app()->make(VideoRepo::class)->find($oldModel->id);

        $this->assertEquals($newModel->size, $request['size']);
        $this->assertEquals($newModel->viewer_count, $request['viewer_count']);
        $newModel->forceDelete();
    }
}
