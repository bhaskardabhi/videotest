<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Repositories\User as UserRepo;
use App\Repositories\Video as VideoRepo;
use App\Models\User;
use App\Models\Video;

class UserRepositoryTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetVideosTotalSize()
    {
        $user = User::has('videos')->inRandomOrder()->first();

        if(!$user){
            $user = User::inRandomOrder()->first();
        }

        $this->assertEquals($user->videos()->sum('size'), app()->make(UserRepo::class)->getVideosTotalSize($user->id));
    }
}
