<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Repositories\User as UserRepo;
use App\Repositories\Video as VideoRepo;
use App\Models\User;
use App\Models\Video;

class VideoRepositoryTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetVideosTotalSizeByUser()
    {
        $user = User::has('videos')->inRandomOrder()->first();

        if(!$user){
            $user = User::inRandomOrder()->first();
        }

        $this->assertEquals($user->videos()->sum('size'), app()->make(VideoRepo::class)->getVideosTotalSizeByUser($user->id));
    }

    public function testFindReturnCorrectModel()
    {
        $video = Video::inRandomOrder()->first();

        if(!$video){
            $video = Video::inRandomOrder()->first();
        }

        $this->assertEquals($video, app()->make(VideoRepo::class)->find($video->id));

        // Check if its working with with parameter
        $video->load('createdBy');

        $this->assertEquals($video, app()->make(VideoRepo::class)->find($video->id,['createdBy']));
    }

    public function testPutFunction()
    {
        $video = factory(Video::class)->create();

        $data = [
            'size' => rand(10, 1000),
            'viewer_count' => rand(10, 1000)
        ];

        $this->assertTrue(app()->make(VideoRepo::class)->put($video->id,$data));

        $video = $video->fresh();

        $this->assertEquals($video->size, $data['size']);
        $this->assertEquals($video->viewer_count, $data['viewer_count']);
        $video->forceDelete();
    }
}
